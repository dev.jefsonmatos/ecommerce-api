class Product < ApplicationRecord
  include NameSearchable
  include Paginatable

  belongs_to :productable, polymorphic: true
  has_many :product_categories, dependent: :destroy
  has_many :categories, through: :product_categories
  has_one_attached :image

  validates :name, uniqueness: { case_sensitive: false }
  validates_presence_of :name, :description, :price
  validates :price, numericality: { greater_than: 0 }
  validates :image, presence: true
  validates :status, presence: true

  enum status: { available: 1, unavailable: 2 }
end
