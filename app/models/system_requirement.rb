class SystemRequirement < ApplicationRecord
  has_many :games, dependent: :restrict_with_error
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates_presence_of :operational_system, :storage, :processor, :memory,
                        :video_board

  include NameSearchable
  include Paginatable
end
