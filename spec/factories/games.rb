FactoryBot.define do
  factory :game do
    mode { %i(pvp pve both).sample }
    release_date { "2021-01-25 16:23:15" }
    developer { Faker::Company.name }
    system_requirement
  end
end
